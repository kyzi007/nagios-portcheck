# -*- coding:utf-8 -*-
import optparse
import os
import re
import subprocess
import sys

__author__ = "kyzi007"


def main():
    op = optparse.OptionParser(
        usage="%prog ip",
    )
    op.add_option(
        '-p',
        '--ports',
        dest='ports',
        default='0-65535',
        help="ports, comma separated or range, default: %default",
    )
    op.add_option(
        '-a',
        '--allow-ports',
        dest='allow_ports',
        default='80',
        help="allow ports, comma separated or range, default: %default",
    )
    op.add_option(
        '-m',
        '--max-rate',
        dest='max_rate',
        default='1000',
        help="scan speed. WARRING! if this value is lager of 100 slow host may be skipped opened port, default: %default",
    )
    op.add_option(
        '-e',
        '--interface',
        dest='interface',
        default=None,
        help="Use specified interface, default: %default",
    )

    opts, args = op.parse_args()

    ip = args[0]

    try:
        ports = scan(
            ip=ip,
            scan_ports=opts.ports,
            allow=opts.allow_ports,
            speed=opts.max_rate,
            interface=opts.interface)
    except Exception as e:
        print (e)
        return 3

    if ports:
        print(
            'Discovered open port{s} {ports}'.format(
                ports=','.join(ports),
                s='s' if len(ports) > 1 else ''
            )
        )
        return 2
    return 0


def scan(ip, scan_ports, allow, speed, interface):
    """
    run masscan scan
    https://github.com/robertdavidgraham/masscan
    """
    command = 'masscan {ip} --ports {ports} --max-rate {speed}'.format(ip=ip, ports=scan_ports, speed=speed)
    if interface:
        command += ' -e {interface}'.format(interface=interface)
    out = run_shell(command)
    opened_ports = re.findall('Discovered open port (\d+)', out)

    return disallow_ports(opened_ports, allow) if allow else opened_ports


def disallow_ports(ports, allow):
    """
    delete allowed ports in list
    """
    ports = map(lambda x: int(x), ports)
    allow_list = map(lambda x: int(x), re.findall('\d+', allow))
    if re.search('\d+-\d+', allow):
        start = allow_list[0]
        end = allow_list[1]
        filtered_ports = filter(lambda x: x >= end or x <= start, ports)
    else:
        filtered_ports = filter(lambda x: x not in allow_list, ports)
    return map(lambda x: str(x), filtered_ports)


def run_shell(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = process.communicate()
    retcode = process.poll()
    if retcode:
        raise Exception('Get subprocess retcode {code} from command "{command}"'.format(code=retcode, command=command))
    return output


if __name__ == '__main__':
    sys.exit(main())
