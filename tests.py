from portcheck import *

assert disallow_ports(['1', '2'], '1,2') == []
assert disallow_ports(['1', '2'], '1,2 ') == []
assert disallow_ports(['1', '2'], '1') == ['2']
assert disallow_ports(['1', '2'], '0') == ['1', '2']
assert disallow_ports(['1', '20'], '0-10') == ['20']
assert disallow_ports(['1', '2'], '0-10') == []
