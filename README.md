Portcheck
=========
This is a plugin for nagios. It scans all ports and returns a list of not allowed ports.

Installation
------------
(https://github.com/robertdavidgraham/masscan)

```sh
$ apt-get install masscan
```

Put portcheck.py in a plugins folder and define command:

```
define command {
        command_name    portcheck
        command_line    python /plugfolder/portcheck.py $HOSTADDRESS$ -e eth0 --allow-ports 80
        }
```

Example
-------

```sh
$ python portcheck.py --help

Usage: portcheck.py ip

Options:
  -h, --help            show this help message and exit
  -p PORTS, --ports=PORTS
                        ports, comma separated or range, default: 0-65535
  -a ALLOW_PORTS, --allow-ports=ALLOW_PORTS
                        allow ports, comma separated or range, default: 80
  -m MAX_RATE, --max-rate=MAX_RATE
                        scan speed. WARRING! if this value is lager of 100
                        slow host may be skipped opened port, default: 1000
  -e INTERFACE, --interface=INTERFACE
                        Use specified interface, default: none

```

```sh
$ sudo python portcheck.py 104.130.43.121 -p 0-7000

Discovered open port 443
```